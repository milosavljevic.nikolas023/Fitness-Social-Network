export class PostModel {
    id: number;
    postName: string;
    post: string;
    likes: number;
    userName: string;
    commentCount: number;
    duration: string;
    like: boolean;
    dislike: boolean;
}