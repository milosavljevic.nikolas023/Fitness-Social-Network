import { LikeType } from './like-type';

export class LikePayload {
    likeType: LikeType;
    postId: number;
}