import { Component, OnInit, Input } from '@angular/core';
import { PostModel } from '../post-model';
import {  faThumbsDown, faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { LikePayload } from './like-payload';
import { LikeType } from './like-type';
import { LikeService } from '../like.service';
import { AuthService } from 'src/app/auth/shared/auth.service';
import { PostService } from '../post.service';
import { throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-like-button',
  templateUrl: './like-button.component.html',
  styleUrls: ['./like-button.component.css']
})
export class LikeButtonComponent implements OnInit {

  @Input() post: PostModel;
  likePayload: LikePayload;
  faArrowUp = faThumbsUp;
  faArrowDown = faThumbsDown;
  likeColor: string;
  dislikeColor: string;
  isLoggedIn: boolean;

  constructor(private likeService: LikeService,
    private authService: AuthService,
    private postService: PostService, private toastr: ToastrService) {

    this.likePayload = {
      likeType: undefined,
      postId: 2
    }
    this.authService.loggedIn.subscribe((data: boolean) => this.isLoggedIn = data);
  }

  ngOnInit(): void {
    this.updateLikeDetails();
  }

  likePost() {
    this.likePayload.likeType = LikeType.LIKE;
    this.like();
    this.dislikeColor = '';
  }

  dislikePost() {
    this.likePayload.likeType = LikeType.DISLIKE;
    this.like();
    this.likeColor = '';
  }

  private like() {
    this.likePayload.postId = this.post.id;
    this.likeService.like(this.likePayload).subscribe(() => {
      this.updateLikeDetails();
    }, error => {
      this.toastr.error(error.error.message);
      throwError(error);
    });
  }

  private updateLikeDetails() {
    this.postService.getPost(this.post.id).subscribe(post => {
      this.post = post;
    });
  }
}
