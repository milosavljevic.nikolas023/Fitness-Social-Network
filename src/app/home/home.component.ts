import { Component, OnInit } from '@angular/core';
import { PostModel } from '../shared/post-model';
import { PostService } from '../shared/post.service';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  posts: Array<PostModel> = [];
  posts1: Array<any> = [];

  constructor(private postService: PostService, private localStorage: LocalStorageService) {
    
  }

  ngOnInit(): void {
    this.getPosts()
  }

  private getPosts(){
    this.posts1 = [];
    this.postService.getAllPosts().subscribe(post => {

      for (let index = post.length-1; index >= 0; index--) {
        this.posts1.push(post[index]);
      }
      this.posts = this.posts1;
    });
  }

  isLoggedUser(): boolean{
    if(this.localStorage.retrieve('username') === null){
      return false;
    }
    return true;
  }
}
