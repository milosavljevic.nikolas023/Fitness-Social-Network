import { Component, OnInit, Input } from '@angular/core';
import { PostService } from 'src/app/shared/post.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CommentService } from 'src/app/comment/comment.service';
import { PostModel } from 'src/app/shared/post-model';
import { CommentPayload } from 'src/app/comment/comment.payload';
import { AuthService } from '../shared/auth.service';
import { LocalStorageService } from 'ngx-webstorage';
import { UserService } from '../users.service';
import { HttpClient } from '@angular/common/http';
import { ImageService } from './image.service';
import { throwError } from 'rxjs';
import { UsersPayload } from '../users.response.payload';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  name: string;
  posts: PostModel[];
  comments: CommentPayload[];
  postLength: number;
  commentLength: number;
  selectedFile: File = null;
  user: UsersPayload;

  constructor(private activatedRoute: ActivatedRoute, private postService: PostService,
    private commentService: CommentService, private authService: AuthService,
    private localStorage: LocalStorageService, private userService: UserService, 
    private http: HttpClient, private imageService: ImageService, private router: Router) {
      this.name = this.activatedRoute.snapshot.params.name;
  }

  ngOnInit(): void {
    
    this.userService.getUserByUsername(this.name).subscribe(data => {
     this.user = data;
    });

   this.postService.getAllPostsByUser(this.name).subscribe(data => {
     this.posts = data;
     this.postLength = data.length;
   });
   this.commentService.getAllCommentsByUser(this.name).subscribe(data => {
     this.comments = data;
     this.commentLength = data.length;
   });
    console.log(this.isLoggedUser());
  }

  isLoggedUser(): boolean{
    if(this.name === this.localStorage.retrieve('username')){
      return true;
    }
    return false;
  }

  onUpload(){
    this.imageService.postImage(this.selectedFile, this.name).subscribe(data => {
      console.log(data);
    }, error => {
      throwError(error);
    });
  }

  onFileSelected(event){
    this.selectedFile = <File> event.target.files[0];
    console.log(this.selectedFile);
  }


}
