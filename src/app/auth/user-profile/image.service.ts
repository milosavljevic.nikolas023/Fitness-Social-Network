import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class ImageService {
    
    constructor(private httpClient: HttpClient) { }

    postImage(image: File, username: string){
        
        const fd = new FormData();
        fd.append('file', image);
        return this.httpClient.post<any>('http://localhost:8080/api/image/'+ username, fd);
    }

}