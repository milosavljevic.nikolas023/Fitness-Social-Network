import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UsersPayload } from '../users.response.payload';
import { UserService } from '../users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-trainees',
  templateUrl: './trainees.component.html',
  styleUrls: ['./trainees.component.css']
})
export class TraineesComponent implements OnInit {

  userForm: FormGroup;
  userPayload: UsersPayload;
  trainees: UsersPayload[];
  userId: number;

  constructor(private userService: UserService, private activateRoute: ActivatedRoute,
    private router: Router) {
      
     }

  ngOnInit(): void {
    this.getTrainees();
  }

  private getTrainees(){
    this.userService.getAllTrainees().subscribe(data => {
      this.trainees = data;
      console.log(data);
    }, error => {
      throwError(error);
    });
  }

  private sendMessage(userId){
this.trainees
  }

  goToUserProfile(userName: string){
    this.router.navigateByUrl('/user-profile/' + userName);
  }

}