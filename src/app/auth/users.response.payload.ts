export class UsersPayload{
    userName: string;
    email: string;
    userId: number;
    firstName:string;
    lastName: string;
}