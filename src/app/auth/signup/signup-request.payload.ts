export interface SignupRequestPayload {
    username: string;
    password: string;
    email: string;
    firstName: string;
    lastName: string;
    isTrainer: boolean;
    about: string;
}
