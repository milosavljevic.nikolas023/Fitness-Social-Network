import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsersPayload } from './users.response.payload';

@Injectable({
    providedIn: 'root'
})

export class UserService {
    
    constructor(private httpClient: HttpClient){

    }

    getAllTrainers(){
        return this.httpClient.get<UsersPayload[]>('http://localhost:8080/api/users/trainers')
    }

    getAllTrainees(){
        return this.httpClient.get<UsersPayload[]>('http://localhost:8080/api/users/trainees')
    }

    getUserByUsername(username: string){
        return this.httpClient.get<UsersPayload>("http://localhost:8080/api/users/" + username)
    }
}