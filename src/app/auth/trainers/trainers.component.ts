import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UsersPayload } from './../users.response.payload';
import { UserService } from './../users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-trainers',
  templateUrl: './trainers.component.html',
  styleUrls: ['./trainers.component.css']
})
export class TrainersComponent implements OnInit {

  userForm: FormGroup;
  userPayload: UsersPayload;
  trainers: UsersPayload[];
  userId:number;

  constructor(private userService: UserService, private router: Router) {
      
     }

  ngOnInit(): void {
    this.getTrainers();
  }

  private getTrainers(){
    this.userService.getAllTrainers().subscribe(data => {
      this.trainers = data;
    }, error => {
      throwError(error);
    });
  }

  private sendMessage(userId){
this.trainers
  }

  goToUserProfile(userName: string){
    this.router.navigateByUrl('/user-profile/' + userName);
  }

}