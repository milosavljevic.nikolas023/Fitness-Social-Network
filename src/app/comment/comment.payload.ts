export class CommentPayload{
    comment: string;
    postId: number;
    username?:string;
    duration?: string;
}