export class MessagePayload{
    message: string;
    conversationId: number;
    username?:string;
    duration?: string;
}