import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessagePayload } from './message.payload';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MessageService{
    

    constructor(private httpClient: HttpClient){

    }

    getAllMessagesForConversation(conversationId: number): Observable<MessagePayload[]>{
        return this.httpClient.get<MessagePayload[]>('http://localhost:8080/api/message/' + conversationId);
    }

    sendMessage(messagePayload: MessagePayload): Observable<any> {
        return this.httpClient.post<any>('http://localhost:8080/api/message', messagePayload);
      }

}