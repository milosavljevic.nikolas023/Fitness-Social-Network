import { Component, OnInit } from '@angular/core';
import { MessagePayload } from './message.payload'
import { MessageService } from './message.service'
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  conversationId: number;
  messageForm: FormGroup;
  messagePayload: MessagePayload;
  messages: MessagePayload[];
  messages1: Array<any> = [];


  constructor(private messageService: MessageService
    , private activateRoute: ActivatedRoute, private router: Router) {
      this.conversationId = this.activateRoute.snapshot.params.id;

      this.messageForm = new FormGroup({
        message: new FormControl('', Validators.required)
      });
      this.messagePayload = {
        message: '',
        conversationId: this.conversationId
      }
     }

  ngOnInit(): void {
    this.getMessages();
  }

  sendMessage(){
    this.messagePayload.message = this.messageForm.get('message').value;
    this.messageService.sendMessage(this.messagePayload).subscribe(data => {
      this.messageForm.get('message').setValue('');
      this.getMessages();
    }, error => {
      throwError(error);
    })
  }

  private getMessages(){
    this.messages1 = [];
    this.messageService.getAllMessagesForConversation(this.conversationId)
      .subscribe(data => {

        for(let index = data.length-1; index >= 0; index--){
          this.messages1.push(data[index]);
        }
        this.messages = this.messages1;
      });

  }
}
